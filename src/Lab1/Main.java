package Lab1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input;
        Game myGame = new Game();

        myGame.printMenu();
        input = scanner.nextInt();
        switch (input) {
            case 1:
                myGame.newBoard();
                myGame.printBoard();
                System.out.println("Grę rozpoczyna gracz O na środkowej planszy");
                myGame.setPlansza(4);
                input = 4;
                do {
                    System.out.println("Gracz O: Jesteś na planszy " + input + ". Wybierz pole");
                    input = scanner.nextInt();
                    myGame.inputToCoordinates(input);
                    myGame.putO();
                    myGame.printBoard();
                    if (myGame.doesOWins()) break;
                    myGame.setPlansza(input - 1);
                    System.out.println("Gracz X: Jesteś na planszy " + input + ". Wybierz pole");
                    input = scanner.nextInt();
                    myGame.inputToCoordinates(input);
                    myGame.putX();
                    myGame.printBoard();
                    if (myGame.doesXWins()) break;
                    myGame.setPlansza(input - 1);
                } while (!myGame.doesOWins() && !myGame.doesXWins() /* warunek, że są jeszcze dostępne ruchy) */);
                myGame.printResults();
                break;
            case 2:
                myGame.newBoard();
                myGame.printBoard();
                System.out.println("Jesteś graczem O. Rozpoczynasz grę na środkowej planszy");
                myGame.setPlansza(4);
                input = 5;
                do {
                    System.out.println("Gracz O: Jesteś na planszy " + input + ". Wybierz pole");
                    input = scanner.nextInt();
                    myGame.inputToCoordinates(input);
                    myGame.putO();
                    myGame.printBoard();
                    if (myGame.doesOWins()) break;
                    myGame.setPlansza(input - 1);
                    System.out.println("Ruch wykonuje komputer");
                    input = myGame.computersChoice();
                    if (input == 0){
                        System.out.println("Brak ruchu. Koniec gry");
                        break;
                    }
                    myGame.inputToCoordinates(input);
                    myGame.putX();
                    myGame.printBoard();
                    if (myGame.doesXWins()) break;
                    myGame.setPlansza(input - 1);
                } while (!myGame.doesOWins() && !myGame.doesXWins() /* warunek, że są jeszcze dostępne ruchy) */);
                myGame.printResults();

                break;
            case 3:
                break;
        }
    }
}

