package Lab1;

public class Game {

    private char[][][] board = new char[9][3][3];

    private int plansza;
    private int wiersz;
    private int kolumna;

    public void setPlansza(int plansza) {
        this.plansza = plansza;
    }

    public void printMenu() {
        System.out.println("Witaj w grze kółko i krzyżyk!");
        System.out.println("1 - Nowa gra dla dwóch osób");
        System.out.println("2 - Zagraj z komputerem");
        System.out.println("3 - Wyjscie");
        System.out.println();
    }

    public void newBoard() {
        for (int i = 0; i < 9; i++) {
            char k = '1';
            for (int j = 0; j < 3; j++) {
                for (int l = 0; l < 3; l++) {
                    board[i][j][l] = k;
                    k++;
                }
            }
        }
    }

    public void printBoard() {
        for (int i = 0; i < 3; i++) { //wiersz
            for (int j = 0; j < 3; j++) {  //plansza
                for (int k = 0; k < 3; k++) {  //kolumna
                    System.out.print(board[j][i][k] + " ");
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < 3; i++) { //wiersz
            for (int j = 3; j < 6; j++) {  //plansza
                for (int k = 0; k < 3; k++) {  //kolumna
                    System.out.print(board[j][i][k] + " ");
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < 3; i++) { //wiersz
            for (int j = 6; j < 9; j++) {  //plansza
                for (int k = 0; k < 3; k++) {  //kolumna
                    System.out.print(board[j][i][k] + " ");
                }
                System.out.print(" ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public void inputToCoordinates( int input) {
        if (input == 1) { wiersz = 0; kolumna = 0;}
        else if (input == 2) {wiersz = 0;; kolumna= 1;}
        else if (input == 3) {wiersz = 0; kolumna = 2;}
        else if (input == 4) {wiersz = 1; kolumna = 0;}
        else if (input == 5) {wiersz = 1; kolumna = 1;}
        else if (input == 6) {wiersz = 1; kolumna = 2;}
        else if (input == 7) {wiersz = 2; kolumna = 0;}
        else if (input == 8) {wiersz = 2;kolumna = 1;}
        else if (input == 9) {wiersz = 2; kolumna = 2;} }

    public void putX() {
        if (board[plansza][wiersz][kolumna] != 'X' && board[plansza][wiersz][kolumna] != 'O') {
            board[plansza][wiersz][kolumna] = 'X';
        }
        else
            System.out.println("Pole jest już zajęte");
    }

    public void putO() {
        if (board[plansza][wiersz][kolumna] != 'X' && board[plansza][wiersz][kolumna] != 'O') {
            board[plansza][wiersz][kolumna] = 'O';
        }
        else
            System.out.println("Pole jest już zajęte");
    }

    public boolean doesXWins () {
        for (int i = 0; i < 3; i++) { // i - kolumny
            if (board[plansza][0][i] == 'X' && board[plansza][1][i] == 'X' && board[plansza][02][i] == 'X')
                return true;
        }
        for (int i = 0; i < 3; i++) { // i - wiersze
            if (board[plansza][i][0] == 'X' && board[plansza][i][1] == 'X' && board[plansza][i][2] == 'X')
                return true;
        }
        if (board[plansza][0][0] == 'X' && board[plansza][1][1] == 'X' && board[plansza][2][2] == 'X')
                return true;
        else if (board[plansza][0][2] == 'X' && board[plansza][1][1] == 'X' && board[plansza][2][0] == 'X')
                return true;
        return false;
    }

    public boolean doesOWins () {

        for (int i = 0; i < 3; i++) { // i - kolumny
            if (board[plansza][0][i] == 'O' && board[plansza][1][i] == 'O' && board[plansza][2][i] == 'O')
                return true;
        }
        for (int i = 0; i < 3; i++) { // i - wiersze
            if (board[plansza][i][0] == 'O' && board[plansza][i][1] == 'O' && board[plansza][i][2] == 'O')
                return true;
        }
        if (board[plansza][0][0] == 'O' && board[plansza][1][1] == 'O' && board[plansza][2][2] == 'O')
            return true;
        else if (board[plansza][0][2] == 'O' && board[plansza][1][1] == 'O' && board[plansza][2][0] == 'O')
            return true;

        return false;
    }

    public void printResults() {
        if (doesXWins()){
            System.out.println("Gratulacje, wygrał gracz X!");
        }
        else if (doesOWins()) {
            System.out.println("Gratulacje, wygrał gracz O!");
        }
        else {
            System.out.println("Remis");
        }
    }

    public int computersChoice() {
        if ((board[plansza][0][1] == 'X' && board[plansza][0][2] == 'X') ||
                (board[plansza][1][0] == 'X' && board[plansza][2][0] == 'X') ||
                (board[plansza][1][1] == 'X' && board[plansza][2][2] == 'X')  )
            return 1;
        else if ((board[plansza][0][0] == 'X' && board[plansza][0][2] == 'X') ||
                (board[plansza][1][1] == 'X' && board[plansza][2][1] == 'X')   )
            return 2;
        else if ((board[plansza][0][0] == 'X' && board[plansza][0][1] == 'X') ||
                (board[plansza][1][1] == 'X' && board[plansza][2][0] == 'X') ||
                (board[plansza][1][2] == 'X' && board[plansza][2][2] == 'X')  )
            return 3;
        else if ((board[plansza][0][0] == 'X' && board[plansza][2][0] == 'X') ||
                (board[plansza][1][1] == 'X' && board[plansza][1][2] == 'X')  )
            return 4;
        else if ((board[plansza][0][1] == 'X' && board[plansza][2][1] == 'X') ||
                (board[plansza][1][0] == 'X' && board[plansza][1][2] == 'X') ||
                (board[plansza][0][0] == 'X' && board[plansza][2][2] == 'X') ||
                (board[plansza][0][2] == 'X' && board[plansza][2][0] == 'X'))
            return 5;
        else if ((board[plansza][1][0] == 'X' && board[plansza][1][1] == 'X') ||
                (board[plansza][0][2] == 'X' && board[plansza][2][2] == 'X')   )
            return 6;
        else if ((board[plansza][0][0] == 'X' && board[plansza][1][0] == 'X') ||
                (board[plansza][2][1] == 'X' && board[plansza][2][2] == 'X') ||
                (board[plansza][1][1] == 'X' && board[plansza][0][2] == 'X')  )
            return 7;
        else if ((board[plansza][2][0] == 'X' && board[plansza][2][2] == 'X') ||
                (board[plansza][0][1] == 'X' && board[plansza][1][1] == 'X')  )
            return 8;
        else if ((board[plansza][0][2] == 'X' && board[plansza][1][2] == 'X') ||
                (board[plansza][2][0] == 'X' && board[plansza][2][1] == 'X') ||
                (board[plansza][0][0] == 'X' && board[plansza][1][1] == 'X')  )
            return 9;
        int k = 1;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[plansza][i][j] != 'X' && board[plansza][i][j] != 'O')
                    return k;
                k++;
            }
        }
        return 0;
    }




}
